#include <iostream>
#include <string>

using namespace std;

string ptb2(double a, double b, double c) {
	if (a == 0) {
		return "KE CHA MI";
	} else {
		double delta = b * b - 4 * a * c;
		if (delta < 0) {
			return "PHUONG TRINH VO NGHIEM";
		} else if (delta == 0) {
			return "PHUONG TRINH CO MOT NGHIEM";
		} else {
			return "PHUONG TRINH CO 2 NGHIEM PHAN BIET";
		}
	}
}

int main() {
	return 0;
}